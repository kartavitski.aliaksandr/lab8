public class Test1 {
    private int a;
    private int b;

    public int sum() {
        return a + b;
    }

    public int getMaxNumber() {
        return a > b ? a : b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.setA(4);
        test1.setB(6);
        System.out.println(test1.getA());
        System.out.println(test1.sum());
        System.out.println(test1.getMaxNumber());
    }
}

