public class Test2 {
    private int a;
    private int b;

    public Test2(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Test2() {
        a = 6;
        b = 8;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public static void main(String[] args) {
        Test2 test2 = new Test2();
        System.out.println(test2.getA());
        Test2 test21 = new Test2(57, 36);
        System.out.println(test21.getA());
    }
}

